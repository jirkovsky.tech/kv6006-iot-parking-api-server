import json
import os
import pathlib

import cv2
import numpy as np


class AIModel:
    MODELS_PATH = os.path.join(pathlib.Path(__file__).parent.resolve(), "models")
    CONF_FILE = "model_conf.json"

    def __init__(self, model_name):
        model_path = os.path.join(self.MODELS_PATH, model_name)
        with open(os.path.join(model_path, self.CONF_FILE), "r") as f:
            config = json.loads(f.read())
        self.pre_defined_confidence = config["confidence"]
        self.pre_defined_threshold = config["threshold"]
        self.labels = config["labels"]

        weights_path = os.path.join(model_path, "yolov3.weights")
        config_path = os.path.join(model_path, "yolov3.cfg")
        self.net = cv2.dnn.readNetFromDarknet(config_path, weights_path)
        self.input_width, self.input_height = config["input_width"], config["input_height"]

    def count_vehicles(self, idxs, boxes, classIDs, vehicle_count, frame):
        list_of_vehicles = ["bicycle", "car", "motorbike", "bus", "truck", "train"]
        current_detections = {}
        if len(idxs) > 0:
            for i in idxs.flatten():
                (x, y) = (boxes[i][0], boxes[i][1])
                (w, h) = (boxes[i][2], boxes[i][3])
                centerX = x + (w // 2)
                centerY = y + (h // 2)
                if (self.labels[classIDs[i]] in list_of_vehicles):
                    current_detections[(centerX, centerY)] = vehicle_count
                    ID = current_detections.get((centerX, centerY))
                    if (list(current_detections.values()).count(ID) > 1):
                        current_detections[(centerX, centerY)] = vehicle_count
                        vehicle_count += 1
                    cv2.putText(frame, str(ID), (centerX, centerY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, [0, 0, 255], 2)
        return vehicle_count, current_detections

    def predict(self, img):
        img_h = img.shape[0]
        img_w = img.shape[1]

        ln = self.net.getLayerNames()
        ln = [ln[i - 1] for i in self.net.getUnconnectedOutLayers()]

        vehicle_count = 0
        boxes, confidences, classIDs = [], [], []

        blob = cv2.dnn.blobFromImage(img, 1 / 255.0, (self.input_width, self.input_height), swapRB=True, crop=False)
        self.net.setInput(blob)
        layerOutputs = self.net.forward(ln)
        for output in layerOutputs:
            for i, detection in enumerate(output):
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                if confidence > self.pre_defined_confidence:
                    box = detection[0:4] * np.array([img_w, img_h, img_w, img_h])
                    (centerX, centerY, width, height) = box.astype("int")
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)
            idxs = cv2.dnn.NMSBoxes(boxes, confidences, self.pre_defined_confidence, self.pre_defined_threshold)
            vehicle_count, current_detections = self.count_vehicles(idxs, boxes, classIDs, vehicle_count, img)
        return vehicle_count
