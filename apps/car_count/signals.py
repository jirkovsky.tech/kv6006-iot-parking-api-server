from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.car_count.models import CountRecord


@receiver(post_save, sender=CountRecord, dispatch_uid="update_segment_count")
def update_segment_count(sender, instance, **kwargs):
    segment = instance.segment
    segment.occupied = instance.count
    segment.last_update = instance.created
    segment.save()
