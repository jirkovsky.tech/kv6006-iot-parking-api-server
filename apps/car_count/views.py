import cv2
import numpy as np
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.car_count.models import Segment, ParkingLot, CountRecord
from apps.car_count.serializers import ParkingLotSerializer, SegmentSerializer, ParkingLotDetailSerializer
from apps.car_count.struct import AIModel
import matplotlib.pyplot as plt


class ParkingLotViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = ParkingLot.objects.all()
    serializer_class = ParkingLotSerializer
    pagination_class = None

    def get_serializer_class(self):
        if (self.request.method == "GET") and self.request.parser_context["kwargs"]:
           return ParkingLotDetailSerializer
        return self.serializer_class


class SegmentViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Segment.objects.all()
    serializer_class = SegmentSerializer
    pagination_class = None

    @action(detail=True, methods=["POST"])
    def count_cars(self, request, parking_lots_pk, pk):
        img_io = request.FILES["media"].file.getvalue()
        img = cv2.imdecode(np.frombuffer(img_io, np.uint8), -1)
        # cv2.imwrite("received_image.jpg", img)
        plt.imshow(img)
        plt.show()
        model = AIModel("yolov3")
        cars_parked = model.predict(img)
        segment = Segment.objects.get(pk=pk)
        CountRecord.objects.create(segment=segment, count=cars_parked)
        data = {
            "segment_occupied": cars_parked,
            "segment_free_spaces": segment.free_spaces,
            "segment_capacity": segment.capacity,
            "parking_lot_name": segment.parking_lot.name
        }
        return Response(data=data, status=status.HTTP_201_CREATED)
