from rest_framework import serializers
import arrow
from apps.car_count.models import ParkingLot, Segment


class ParkingLotSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParkingLot
        fields = "__all__"


class ParkingLotDetailSerializer(serializers.ModelSerializer):
    capacity = serializers.SerializerMethodField()
    free_spaces = serializers.SerializerMethodField()
    last_update = serializers.SerializerMethodField()

    def get_free_spaces(self, obj):
        return obj.free_spaces

    def get_capacity(self, obj):
        return obj.capacity

    def get_last_update(self, obj):
        return arrow.get(obj.updated).format("HH:mm DD/MM/YYYY")

    class Meta:
        model = ParkingLot
        fields = "__all__"


class SegmentSerializer(serializers.ModelSerializer):
    free_spaces = serializers.SerializerMethodField()
    last_updated = serializers.SerializerMethodField()

    def get_free_spaces(self, obj):
        return obj.free_spaces

    def get_last_updated(self, obj):
        return arrow.get(obj.last_update).format("HH:mm DD/MM/YYYY")

    class Meta:
        model = Segment
        fields = '__all__'
